FROM node:boron

EXPOSE 3000

WORKDIR /usr/src/app
COPY package.json ./
RUN npm install

COPY app.js ./

CMD [ "npm", "start" ]