const express = require('express');

// Constants
const PORT = 3000;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/:path', (req, res) => {
	let response = '<table><tr><th>name</th><th>value</th></tr>';
	response += `<tr><td>Path</td><td>${req.path}</td></tr>`;
	response += `<tr><td>------</td><td>------</td></tr>`;
	Object.keys(req.headers).map(k => {
		response += `<tr><td>${k}</td><td>${req.header(k)}</td></tr>`;
	});
	response += '</table>'
	res.send(response);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);